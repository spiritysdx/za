# za

原始仓库：https://github.com/spiritLHLS/ecs

本仓库为备份仓库/开发仓库，日常使用与说明详见原始仓库。

以下为开发使用的测试脚本，开发使用，常人勿要使用

```bash
curl -L https://gitlab.com/spiritysdx/za/-/raw/main/test.sh -o test.sh && chmod +x test.sh && bash test.sh && rm -rf test.sh
```

```bash
curl -L https://gitlab.com/spiritysdx/za/-/raw/main/yabsiotest.sh -o yabsiotest.sh && chmod +x yabsiotest.sh && bash ./yabsiotest.sh && rm -rf yabsiotest.sh
```

# 单项测试

```bash
curl -L https://gitlab.com/spiritysdx/za/-/raw/main/test1.sh -o test1.sh && chmod +x test1.sh && dos2unix test1.sh && bash test1.sh && rm -rf test1.sh
```
